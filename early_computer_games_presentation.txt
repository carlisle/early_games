Games on computers have been around since computer and have pushed the computation capabilites and have let to the creation 
of new hardware and software.


Pre-history


Teletype / Teleprinter

Starting with the development of the telegraph and Morse Code in 1838 there were a series of technological steps that let to the creating of 
the teletype interface by the early 20th century.

This is important because many of the earliest games that were widely used were created for teletype interfaces as computer displays were rare.

https://en.wikipedia.org/wiki/Teleprinter


Cathode Ray Tube Display
A type of vaccum tube
1890s

CRT TV recivers developed in 1920
many including Philo Farnsworth
Oscilloscope CRTs  1920s-1930s
Radar CRTs  1940s


1947 - CRT amusement device
Developed but never produced
https://en.wikipedia.org/wiki/Cathode-ray_tube_amusement_device


Time sharing operating system  1959-1964

The first computers ran jobs on a batch system and did not allow for interactivity with a user until time sharing systems were created.

> https://en.wikipedia.org/wiki/Time-sharing

IBM TSO for OS/MVT
DEC PDP-6 Time sharing monitor
MIT Time sharing system for PDP-1
MIT Compatible Time Sharing System -> Multics -> AT&T Unix

> https://en.wikipedia.org/wiki/Incompatible_Timesharing_System

References

https://en.wikipedia.org/wiki/Early_history_of_video_games

https://videogamehistorian.wordpress.com/table-of-contents/

---

1958 & 1959 - Tennis for Two
William Higinbothan 
Brookhaven National Laboratory, Long Island NY
Visitor Exhibit

> https://en.wikipedia.org/wiki/Tennis_for_Two

https://www.youtube.com/watch?v=s2E9iSQfGdg

https://www.youtube.com/watch?v=6PG2mdU_i8k

More info: When Games Went Click: The Story of Tennis for Two

> https://www.youtube.com/watch?v=6QSHZ20MQfE


> https://en.wikipedia.org/wiki/William_Higinbotham  1910-1994

https://videogamehistorian.wordpress.com/tag/robert-dvorak/


---

1962 - Spacewar!

Digital Electronic Corporation created the Program Data Processor 1 computer in 1958 as a scientific computer with features different 
than IBM Mainframes common at the time. It included paper tape to record and load programs and an optional crt display.
9.2 KB ram, cpu around 5Hz

MIT Open House on DEC PDP-1 

Two people using a single keyboard and interactive display could control two spacecraft to battle each other.

The left side user used keys W A S D to manuver their spacecraft the "wedge"
THe right side user used keys I J K L to manuver their spacecraft the "needle"

These keys are still used today.

The wedge was reused by Atari for Atari Spacewar and Asteroids

> https://en.wikipedia.org/wiki/Spacewar!

> https://en.wikipedia.org/wiki/Steve_Russell_(computer_scientist) 1937

> https://en.wikipedia.org/wiki/Tech_Model_Railroad_Club

Spacewar! simulator:

https://www.masswerk.at/spacewar/

Using a restored PDP-1 at the Computer History Museum in Mountain View CA
including playing SpaceWar!
https://www.youtube.com/watch?v=1EWQYAfuMYw


---

1969 Lunar Lander
PDP-8
Originally written by Jim Storer on a PDP-8 while in high school

A text based game where a user could input thrust and was returned altitue and velocity.
The goal was the land on the moon with minimal velocity before running out of fuel.

Later converted to video game around 1973 with horizontal motion.

https://en.wikipedia.org/wiki/Lunar_Lander_(video_game_genre)

Notes by Jim Storer
https://www.cs.brandeis.edu/~storer/LunarLander/LunarLander.html

Here is a re-creation of the original text game
https://www.cs.drexel.edu/~vck29/newlunarlander.html

Here is a recreation of the video game
http://moonlander.seb.ly/


----

1971  Star Trek
University of California, Irvine
Mike Mayfield and others

> https://en.wikipedia.org/wiki/Star_Trek_(1971_video_game)



Mike Mayfield wrote the game in BASIC for the SDS Sigma 7 mainframe
computer with the goal of
creating a game like Spacewar! (1962) that could be played with a
teleprinter instead of a graphical
display.


> http://www.catb.org/~esr/super-star-trek/sst-doc.html
> https://gitlab.com/esr/super-star-trek
https://gitlab.com/gitlab-org/gitlab-foss/issues/63512

Earliest copy of the original game can be found here:
> https://gitlab.com/esr/super-star-trek/blob/master/historic/mayfield.basic


Hints:

Scan: on many versions its SRSCAN or just S, on this one it's CHART
which shows short and long range
scan or just C

Moving:
Impulse: to move on the short range scan use MOVE or just M

Warp:

m a 1 2

m a 2 7 7 1

Phasers


> https://almy.us/sst.html

mkdir sst
cd sst
wget https://almy.us/files/sstsrc.zip
unzip sstsrc.zip
make
./sst

Python version:  ( Who is esr ? )

git clone https://gitlab.com/esr/super-star-trek.git
cd super-star-trek/
./sst.py


Playing these games on a teletype:

https://twitter.com/stevenplin/status/1025789004543709184?lang=en

----

1971 The Oregon Trail
HP 2100 minicomputer
Carleton College Minnesota
Minnesota Educational Computing Consort

https://github.com/fortran-gaming/oregon-trail-1975



----

1973 Hunt the Wumpus
University of Massachusetts Dartmouth

> https://en.wikipedia.org/wiki/Hunt_the_Wumpus

> https://en.wikipedia.org/wiki/Gregory_Yob  1945 - 2005

https://www.youtube.com/watch?v=fdZHpZBrFTc


http://catb.org/~esr/wumpus/

----

Earliest Commercial Games-



* Magnavox Odyssey
First Home Console Sept 1972
Light gun

https://en.wikipedia.org/wiki/Ralph_H._Baer 1922 - 2014


* Atari

https://en.wikipedia.org/wiki/Nolan_Bushnell  b 1943

1972 Atari Pong Arcade Game

https://en.wikipedia.org/wiki/Pong

https://en.wikipedia.org/wiki/Allan_Alcorn


1975 Atari Sears Pong Home Game
> https://www.youtube.com/watch?v=pifYfvHDHGo

1977 - Atari Space Wars Arcade Game

> https://en.wikipedia.org/wiki/Space_Wars

> https://www.youtube.com/watch?v=3J6vo6WCeOE

> https://www.youtube.com/watch?v=pNNrzEGgpCs

1978 - Taito / Midway Space Invaders

> https://en.wikipedia.org/wiki/Space_Invaders

1979 - Atari Lunar Lander Arcade Game

> http://moonlander.seb.ly/

1979 Atari Asteroids

https://games.aarp.org/games/atari-asteroids



----


1976 Colossal Cave Adventure

PDP-10 originally written in FORTRAN


> https://en.wikipedia.org/wiki/Colossal_Cave_Adventure

Earliest Interactive Fiction

https://en.wikipedia.org/wiki/William_Crowther_(programmer)  1936

Bill and his wife, Pat had been prolific cavers. He created the game after a divorce
as a way to connect with his kids. Based on Colosal Cave, Kentucky with added fantasy
elements after playing D&D.

https://en.wikipedia.org/wiki/Don_Woods_(programmer)  1954


http://www.amc.com/shows/halt-and-catch-fire/exclusives/colossal-cave-adventure

Suggestioned commands to get started:
LOOK, GO BUILDING, GET LAMP, GET KEYS, GET FOOD, GET WATER, GO OUTSIDE, FOLLOW STREAM,
GO SOUTH, UNLOCK GRATE, DOWN

If you want to know more about these type of games search for the
documentary: Get Lamp
http://www.getlamp.com/

https://www.youtube.com/watch?v=LRhbcDzbGSU

The original source code was written in fortran is difficult to
install on modern operating systems.

Original source code is found here:

https://jerz.setonhill.edu/intfic/colossal-cave-adventure-source-code/
http://www.icynic.com/~don/jerz/

An effort by Eric Raymond to make modify the original code only enough
to compile on modern
computers is here:

https://gitlab.com/esr/open-adventure

I have confirmed that this code can be downloaded and compiled on modern linux
systems. Download the tar archive and read the install instructions here:

http://www.catb.org/~esr/open-adventure/


also see http://www.digitalhumanities.org/dhq/vol/1/2/000009/000009.html

Players of Adventure went on to create their own games and the first game companies 
as Personal Computers became common including:

Infocom  1979 by several former MIT student as a general programming company 
produced commercial versions of Zork

On Line Systems / Sierra Online / Sierra 1979 by Ken and Roberta Williams

First new game in 25 years: 
https://www.colossalcave3d.com/


----

1977 - Zork
MIT
MDL programming language on DEC PDP-10

 > https://en.wikipedia.org/wiki/Zork

let to the founding of Infocom in 1979

> https://en.wikipedia.org/wiki/Infocom

----

1977 - Empire

Walter Bright and others


Turn based computer game based on board games such as Risk! and Battle for Britian

Walter Bright created Empire as a board wargame as a child, inspired by Risk, Stratego, and the film Battle of Britain. 
He found gameplay tedious, but later realized that a computer could handle the gameplay and serve as CPU opponent.
The initial version of computer Empire was written in BASIC, before being re-written around 1977 in the FORTRAN programming language for the PDP-10 computer at Caltech. 

https://en.wikipedia.org/wiki/Empire_(1977_video_game)

Read the instructions before playing: 

man empire

To play on compile:

empire


----

1980 - Rogue

University of California Santa Cruz - Go Slugs!
PDP-11, VAX-11, BSD


> https://en.wikipedia.org/wiki/Rogue_(video_game)

“Among its fans included UNIX's co-developer Ken Thompson working at
Bell Labs; Dennis Ritchie had
joked at the time that Rogue was "the biggest waste of CPU cycles in history"


> https://en.wikipedia.org/wiki/Glenn_Wichman  1960

> https://en.wikipedia.org/wiki/Ken_Arnold  1958


1. Create a directory called "src" under your home directory

2. Change to that directory and download a file using either method A
or method B

Method A,
Step 1: Select one of these urls to download the source code:


http://pkgs.fedoraproject.org/repo/pkgs/rogue/rogue5.4.4-src.tar.gz/033288f46444b06814c81ea69d96e075/rogue5.4.4-src.tar.gz

Step 2: Verify the checksum

033288f46444b06814c81ea69d96e075  rogue5.4.4-src.tar.gz

Step 3: Extract the contents from rogue5.4.4-src.tar.gz

Method B
use git:

git clone https://github.com/phs/rogue

mv rogue rogue5.4.4



3. Change to the rogue5.4.4 directory

4. Run configure and set the target of installation to be the
directory "rogue" under your home
directory /home/<account>/rogue5.4.4

6. Make and install the code using the steps of the GNU Build System

7. Determine if the rogue program is in your path

8. Correct an error in how the man pages were installed:
change to $HOME/rogue5.4.4/share/man
make the directory "man6"
move the file rogue.6 to the directory man6

9. Create a file with all the environmental variables needed to run
this program at
$HOME/rogue5.4.4/rogue_env.sh Append the PATH to include the rogue
binary and also export the
MANPATH varible and set it to $HOME/rogue5.4.4/share/man

10. source the file created above

11. run rogue

12. Enjoy

----



----


> https://www.maketecheasier.com/play-games-linux-terminal/



Look for the bsd-games package on linux

adventure
trek
hangman
wump
ninvaders

Jason Scott

> https://archive.org/

> https://github.com/historicalsource?tab=repositories

Eric Raymond

> http://catb.org/~esr/software.html

> https://www.pcmag.com/feature/352798/the-forgotten-world-of-teletype-computer-games/7

Next: Online Games

https://en.wikipedia.org/wiki/History_of_online_games